package com.epam.trainings.mvc.commands;

import com.epam.trainings.mvc.model.Fibonacci;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class FibonacciCallableCommand implements Command {

  public void execute() {
    ExecutorService executor = Executors.newFixedThreadPool(2);
    try {
      int result = executor.invokeAll(init()).stream().mapToInt(this::getValue).sum();
      System.out.println(result);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    executor.shutdown();
  }

  private int getValue(Future<Integer> future) {
    try {
      return future.get();
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    }
    return 0;
  }

  private List<Callable<Integer>> init() {
    final int[] array = {6, 3, 56, 42};
    return Arrays.stream(array)
        .mapToObj(value -> (Callable<Integer>) () -> new Fibonacci(value).get(value))
        .collect(Collectors.toList());
  }
}

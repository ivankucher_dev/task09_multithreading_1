package com.epam.trainings.mvc.commands;

import com.epam.trainings.mvc.model.Fibonacci;

public class FirstFibonacciCommand implements Command {
  public void execute() {
    int value = 13;
    new Thread(() -> System.out.println(new Fibonacci(value).get(value))).start();
  }
}

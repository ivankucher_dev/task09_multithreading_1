package com.epam.trainings.mvc.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PingPongCommand implements Command {

  private static final Logger log = LogManager.getLogger();
  private Object monitor = new Object();
  private static final long SLEEP = 1000;
  private Thread ping;
  private Thread pong;

  public void execute() {
    setupPingThread();
    setupPongThread();
    ping.start();
    pong.start();
    try {
      Thread.sleep(SLEEP);
    } catch (Exception e) {
      log.error(e);
    }
    ping.interrupt();
    pong.interrupt();
  }

  private void setupPingThread() {
    ping =
        new Thread(
            () -> {
              synchronized (monitor) {
                while (!Thread.interrupted()) {
                  try {
                    monitor.wait();
                    log.info("ping thread");
                    monitor.notify();
                  } catch (InterruptedException e) {
                    log.error(e);
                  }
                }
              }
            });
  }

  private void setupPongThread() {
    pong =
        new Thread(
            () -> {
              synchronized (monitor) {
                while (!Thread.interrupted()) {
                  try {
                    monitor.notify();
                    log.info("pong thread");
                    monitor.wait();
                  } catch (InterruptedException e) {
                    log.error(e);
                  }
                }
              }
            });
  }
}

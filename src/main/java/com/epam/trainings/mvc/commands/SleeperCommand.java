package com.epam.trainings.mvc.commands;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SleeperCommand implements Command {

  public void execute() {
    ScheduledExecutorService executorService = Executors.newScheduledThreadPool(2);
    for (int i = 0; i < 3; i++) {
      int sleep = new Random().nextInt(10) + 1;
      executorService.schedule(() -> System.out.println(sleep), sleep, TimeUnit.SECONDS);
    }
    executorService.shutdown();
  }
}

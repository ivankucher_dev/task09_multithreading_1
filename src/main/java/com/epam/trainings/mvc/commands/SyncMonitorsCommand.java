package com.epam.trainings.mvc.commands;

import com.epam.trainings.mvc.model.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SyncMonitorsCommand implements Command {

  private static Logger log = LogManager.getLogger(SyncMonitorsCommand.class.getName());
  private Object monitorFirst;
  private Object monitorSecond;
  private Object monitorThird;

  public void execute() {
    monitorFirst = new Object();
    monitorSecond = new Object();
    monitorThird = new Object();
    new Thread(this::printA).start();
    new Thread(this::printB).start();
    new Thread(this::printC).start();
  }

  public void printA() {
    synchronized (monitorFirst) {
      try {
        Thread.sleep(3000);
      } catch (InterruptedException e) {
        log.error(e);
        e.printStackTrace();
      }
      log.info("sync fist monitor");
    }
  }

  public void printB() {
    synchronized (monitorSecond) {
      try {
        Thread.sleep(3000);
      } catch (InterruptedException e) {
        log.error(e);
        e.printStackTrace();
      }
      log.info("sync second monitor");
    }
  }

  public void printC() {
    synchronized (monitorThird) {
      try {
        Thread.sleep(3000);
      } catch (InterruptedException e) {
        log.error(e);
        e.printStackTrace();
      }
      log.info("sync third monitor");
    }
  }
}

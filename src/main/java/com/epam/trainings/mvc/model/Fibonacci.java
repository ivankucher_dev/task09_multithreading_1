package com.epam.trainings.mvc.model;

public class Fibonacci {
  private int n;

  public Fibonacci(int n) {
    this.n = n;
  }

  public int get(int n) {
    if (n <= 1) {
      return n;
    }
    return get(n - 1) + get(n - 2);
  }
}

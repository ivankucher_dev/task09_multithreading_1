package com.epam.trainings.mvc.view;

import com.epam.trainings.mvc.commands.*;
import com.epam.trainings.mvc.controller.ViewController;
import com.epam.trainings.mvc.model.Menu;
import com.epam.trainings.utils.PropertiesReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.InputMismatchException;
import java.util.Scanner;

public class View {

  private Menu menu;
  private ViewController controller;
  private Scanner scanner;
  private static Logger log = LogManager.getLogger(Menu.class.getName());

  public View(Menu menu) {
    this.menu = menu;
    scanner = new Scanner(System.in);
  }

  public void startBySettingUpController(ViewController controller) {
    this.controller = controller;
    createMenu();
    show();
  }

  public void updateView() {
    show();
  }

  private void show() {
    menu.getMenuAsString().forEach((k, v) -> System.out.println(k + "." + v));
    int index = -1;
    try {
      index = scanner.nextInt();
    } catch (InputMismatchException e) {
      log.error(e);
      show();
    }
    controller.execute(menu.getCommand(index));
  }

  private void createMenu() {
    menu.add(1, PropertiesReader.getProperty("ping_pong_command"), new PingPongCommand());
    menu.add(2, PropertiesReader.getProperty("first_fibonacci_command"), new FirstFibonacciCommand());
    menu.add(3,PropertiesReader.getProperty("callable_fibonacci_command"), new FibonacciCallableCommand());
    menu.add(4,PropertiesReader.getProperty("sleeper_command"),new SleeperCommand());
    menu.add(5, PropertiesReader.getProperty("syncmonitors_command"), new SyncMonitorsCommand());
  }
}
